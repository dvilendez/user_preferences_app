import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserPreferences {

  static final UserPreferences _instance = new UserPreferences._internal();

  factory UserPreferences () {
    return _instance;
  }

  UserPreferences._internal();

  SharedPreferences _prefs;

  initPrefs() async {
    _prefs = await SharedPreferences.getInstance();
  }

  //None of this properties are used
  // bool   _secondaryColor;
  // int    _gender;
  // String _name;

  // GET and SET gender
  get gender {
    return _prefs.getInt('gender') ?? 1;
  }
  
  set gender ( int value ) {
    _prefs.setInt('gender', value);
  }

  // GET and SET secondaryColor
  get secondaryColor {
    return _prefs.getBool('secondaryColor') ?? false;
  }
  
  set secondaryColor ( bool value ) {
    _prefs.setBool('secondaryColor', value);
  }

  // GET and SET name
  get name {
    return _prefs.getString('name') ?? '';
  }
  
  set name ( String value ) {
    _prefs.setString('name', value);
  }

  // GET and SET lastPage
  get lastPage {
    return _prefs.getString('lastPage') ?? 'home';
  }
  
  set lastPage ( String value ) {
    _prefs.setString('lastPage', value);
  }


}